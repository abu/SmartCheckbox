<?php

namespace Kanboard\Plugin\SmartCheckbox;

use Kanboard\Core\Plugin\Base;

class Plugin extends Base
{
    public function initialize()
    {
        $this->template->hook->attach('template:config:application', 'SmartCheckbox:config/config');

        switch($this->configModel->get('smcbx_activate')) {
            case '':
                $this->configModel->save(array('smcbx_activate' => 1));
                // no break
            case '1':
                $this->hook->on('template:layout:js', array('template' => "plugins/SmartCheckbox/Assets/js/SmartCheckbox.js"));
                break;
        }
    }

    public function getPluginName()
    {
        return 'SmartCheckbox';
    }

    public function getPluginAuthor()
    {
        return 'Alfred Bühler';
    }

    public function getPluginVersion()
    {
        return '0.1.1';
    }

    public function getPluginDescription()
    {
        return 'This plugin cures the checkbox/post misery. Please Consult README for details.';
    }

    public function getPluginHomepage()
    {
        return 'https://codeberg.org/abu/SmartCheckbox';
    }
}
