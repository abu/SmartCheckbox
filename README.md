
# SmartCheckbox

**[Kanboard](https://kanboard.org) plugin for fixing the checkbox/post misery.**

## Purpose, the issue

Any checkbox in a HTML form is only uploaded to the server in its checked state. If unchecked, the state is omitted when uploading the form. As a consequence of this fact, checkboxes cannot be used, e.g. in setting dialogs, at least not without further actions. An option is, adding a hidden _<input\>_ tag with attribute _value=0_ for each checkbox or creating an own ConfigController and handle the issue there. But for using just one or few checkboxes, this is surely an overkill.

## The solution

This plugin contains a JavaScript that installs an event listener for the submit event to all post-forms on a page. Before submitting the form to the server, the script adds a hidden _\<input\>_ tag with attribute _value=0_ for each checkbox in unchecked state.

With this, the built-in ConfigController can save the state of checkboxes without any further problem. No need to favor radio buttons over checkboxes, no need to implement an own ConfigController, just to make checkboxes working.

## Configuration

The plugin’s script is activated by default. I don't expect any unwanted effects, but in case, is can be disabled via Application settings.

## Installation

You have the choice between 3 methods:

1. Install the plugin from the Kanboard plugin manager in one click
2. Download the package and decompress everything under the directory `plugins`
3. Clone this repository into the folder `plugins/SmartCheckbox`.

Note: Plugin folder is case-sensitive.

## Authors & Contributors
- [Alfred Bühler](https://codeberg.org/abu) - Author

## License
- This project is distributed under the [MIT License](https://choosealicense.com/licenses/mit/ "Read The MIT license")
