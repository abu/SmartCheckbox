
'strict';

$(function () {
    $('form[method=post]').submit(function () {
        const form = this;
        $(form).find('input[type=checkbox]').each(function () {
            if (!this.checked) {
                // console.log(`Need to patch ${this.name}`);
                const dummy = document.createElement('input');
                dummy.name = this.name;
                dummy.value = 0;
                dummy.hidden = true;
                form.append(dummy);
            }
        });
    });
});
