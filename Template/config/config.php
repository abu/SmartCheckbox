<fieldset>
    <legend><?= t('SmartCheckbox') ?></legend>
    <?= $this->form->checkbox ('smcbx_activate', t('Activate'),
        1, isset($values['smcbx_activate']) && $values['smcbx_activate'] == 1) ?>
</fieldset>
